import sys
from class_accountant import Accountant

arguments = sys.argv[2:5]

if len(sys.argv) != 5:
    print("Podano złą ilość argumentów")
else:
    filepath = Accountant(sys.argv[1], 'zakup')
    filepath.show_results()
    filepath.sum_of_account()
    filepath.show_warehouse()
    answer = filepath.purchase_sale_action(arguments)
    if answer:
        print(filepath.add_to_base(arguments))
        filepath.write_results()
    else:
        print('Nastapi koniec programu')
