import sys
from class_accountant import Accountant

arguments = sys.argv[2:4]

if len(sys.argv) != 4:
    print("Podano złą ilość argumentów")
else:
    filepath = Accountant(sys.argv[1], 'przeglad')
    filepath.show_results()
    filepath.overview(sys.argv[2], sys.argv[3])
