import sys
from class_accountant import Accountant

if len(sys.argv) != 2:
    print("Podano złą ilość argumentów")
else:
    filepath = Accountant(sys.argv[1], 'konto')
    filepath.show_results()
    print('Stan konta to: ')
    print(filepath.sum_of_account())
