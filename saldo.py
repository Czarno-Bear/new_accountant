import sys
from class_accountant import Accountant

arguments = [sys.argv[2], sys.argv[3]]

if len(sys.argv) != 4:
    print("Podano złą ilość argumentów")
else:
    filepath = Accountant(sys.argv[1], 'saldo')
    print(filepath.show_results())
    print(filepath.add_to_base(arguments))
    filepath.sum_of_account()
    filepath.write_results()
