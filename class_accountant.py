class Accountant:

    # W init pobranie jako parametry nazwy pliku do odczytu oraz nazwy pliku
    # wlasnego. Zainicjowanie listy 'self.write_base' (historii odczytywanej
    # z pliku). Zainicjowanie stanu konta (account_sum) oraz magazynu(warehouse)

    def __init__(self, ac_file_txt, file_name):
        self.ac_file_txt = ac_file_txt
        self.file_name = file_name
        self.write_base = []
        self.account_sum = 0
        self.warehouse = {}

    # Odczyt z pliku account.txt do listy 'write_base'

    def show_results(self):
        with open(self.ac_file_txt, 'r') as fd:
            for line in fd:
                if line != 'stop':
                    self.write_base.append(line.rstrip())
                else:
                    self.write_base.append('stop')
            return self.write_base

    # Dodanie do listy 'write.base' argumentow podawanych w poszczegolnych
    # plikach (saldo, zakup, sprzedaz)

    def add_to_base(self, args):
        for line in self.write_base:
            if line != 'stop':
                continue
            else:
                self.write_base.remove('stop')
                print(self.file_name)
                self.write_base.append(self.file_name)
                i = 0
                while len(args) > i:
                    print(args[i])
                    self.write_base.append(args[i])
                    i += 1
                self.write_base.append('stop')
                break
        return self.write_base

    # Dodanie do magazynu produktów z listy 'write_base', oraz w zaleznosci
    # od wybranej akcji (zakup, sprzedaz) odpowiednie ich zliczenie

    def show_warehouse(self):
        i = 0
        while i < len(self.write_base):
            if self.write_base[i] == 'zakup':
                product = self.write_base[i + 1]
                quantity = int(self.write_base[i + 3])
                if self.warehouse.get(product):
                    self.warehouse[product] += quantity
                else:
                    self.warehouse[product] = quantity
            elif self.write_base[i] == 'sprzedaz':
                product = self.write_base[i + 1]
                quantity = int(self.write_base[i + 3])
                if self.warehouse.get(product) and self.warehouse.get(product) \
                        >= quantity:
                    self.warehouse[product] -= quantity
            i += 1

    # Sprawdzenie czy dany produkt jest w magazynie

    def is_in_warehouse(self, args):
        i = 0
        while i < len(args):
            print(args[i])
            print(self.warehouse[args[i]])
            i += 1

    # Na podstawie listy 'write_base' zsumowanie stanu konta

    def sum_of_account(self):
        i = 0
        while i < len(self.write_base):
            if self.write_base[i] == 'saldo':
                self.account_sum += int(self.write_base[i + 1])
            elif self.write_base[i] == 'zakup':
                self.account_sum -= int(self.write_base[i + 2]) * int(
                    self.write_base[i + 3])
            elif self.write_base[i] == 'sprzedaz':
                self.account_sum +=\
                    int(self.write_base[i + 2]) * int(self.write_base[i + 3])
            elif self.write_base[i] == 'stop':
                return self.account_sum

            i += 1

    # Wpisaniu do pliku listy 'write_base' po wszystkich wykonanych akcjach

    def write_results(self):
        with open(self.ac_file_txt, 'w') as fd:
            i = 0
            while self.write_base[i]:
                if self.write_base[i] == 'stop':
                    fd.write(str(self.write_base[i]))
                    break
                else:
                    fd.write(str(self.write_base[i] + '\n'))
                    i += 1

    # Obsluga zakupu/sprzedazy towarów. W zaleznosci od wybranej akcji,
    # odpowiednie dzialania na liscie 'write_base', na koncie 'account_sum'
    # oraz w magazynie 'warehouse'. Obsluga bledow.

    def purchase_sale_action(self, args):

        product = args[0]
        price = int(args[1])
        quantity = int(args[2])

        sum_money_product = price * quantity

        if self.file_name == 'zakup':
            if sum_money_product > self.account_sum:
                print('Blad! Kwota wieksza niż saldo!')
                return False
            else:
                if product not in self.warehouse:
                    self.warehouse[product] = quantity
                else:
                    self.warehouse[product] += quantity
                return True
        elif self.file_name == 'sprzedaz':
            if self.warehouse[product] < quantity:
                print('Blad! Brak w magazynie! 153')
                return False
            else:
                self.warehouse[product] -= quantity
                return True

    # Funkcja do wyswietlania przegladu

    def overview(self, _from, _to):
        if int(_from) < 0 or int(_to) > len(self.write_base):
            print('Blad! podano zle argumenty')
        else:
            _base = self.write_base[int(_from):int(_to)]
            for v in _base:
                print(v)

# python saldo.py accountant.txt 10000 "na kiciusia"
